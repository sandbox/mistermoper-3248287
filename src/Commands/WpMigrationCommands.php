<?php

namespace Drupal\wp_migration\Commands;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\StatementInterface;
use Drush\Commands\DrushCommands;
use Drupal\Core\File\FileSystemInterface;

/**
 * A Drush commandfile.
 */
class WpMigrationCommands extends DrushCommands {

  /**
   * Used to save in a file the generated csv.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * WpMigrationCommands constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * Command description here.
   *
   * @param string $domain
   *   Domain which has the files we want extract.
   * @param string $destination
   *   Where to dump the file list.
   * @param string $database
   *   Wordpress database.
   * @param string $content_types
   *   List of content types we want extract its files.
   * @param array $options
   *   Options.
   *
   * @option status
   *   If TRUE, filters only by published posts.
   * @option table-prefix
   *   Table prefix.
   * @option extensions
   *   File extensions we want to extract.
   *
   * @usage wp_migration-commandName foo
   *   Usage description
   *
   * @command wp-migration:extract-posts-file-urls
   */
  public function commandName(
      $domain,
      string $destination,
      string $database,
      $content_types = '',
      array $options = [
        'status' => '',
        'table-prefix' => 'wp',
        'extensions' => '',
      ]
    ) {
    $posts_result = $this->getWordpressPostsContent($database, $content_types, $options);
    $urls = $this->extractPostUrls($posts_result, $domain, $options);
    $this->saveUrls($urls, $destination);
  }

  /**
   * Get all wordpress content.
   *
   * @param string $database
   *   Database name.
   * @param string $content_types
   *   Content types.
   * @param array $options
   *   Options.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   Query results.
   */
  protected function getWordpressPostsContent(string $database, string $content_types, array $options) : StatementInterface {
    $database = Database::getConnection('default', $database);
    $posts_query = $database
      ->select($options['table-prefix'] . '_posts', 'wp')
      ->fields('wp', ['post_content']);
    if (!empty($content_types)) {
      $posts_query->condition('wp.post_type', explode(',', $content_types), 'IN');
    }

    if ($options['status']) {
      $posts_query->condition('wp.post_status', explode(',', $options['status']), 'IN');
    }

    return $posts_query->execute();
  }

  /**
   * Extract all post urls.
   *
   * @param \Drupal\Core\Database\StatementInterface $posts_results
   *   Posts results.
   * @param string $domain
   *   Domain of the urls.
   * @param array $options
   *   Options.
   *
   * @return array
   *   List of post urls.
   */
  protected function extractPostUrls(StatementInterface $posts_results, string $domain, array $options) {
    $domain_for_regexp = str_replace('.', '\.', $domain);
    $regexp = '/http(s)?:\/\/' . $domain_for_regexp . '\/wp-content\/uploads\/([0-9]*)\/([0-9]*)\/([^"]*\.[a-z]+)/m';
    while ($post = $posts_results->fetch()) {
      $post_content = urldecode($post->post_content);
      preg_replace_callback($regexp, function ($parts) use (&$urls) {
        $urls[] = reset($parts);
        return NULL;
      }, $post_content);
    }

    $urls = array_unique($urls);

    if (!empty($options['extensions'])) {
      $extensions = explode(',', $options['extensions']);

      $urls = array_filter($urls, function ($url) use ($extensions) {
        return in_array(pathinfo($url, PATHINFO_EXTENSION), $extensions);
      });
    }

    return $urls;
  }

  /**
   * Save all the urls into a csv file.
   *
   * @param array $urls
   *   Urls.
   * @param string $destination
   *   Destination of the file.
   *
   * @throws \Exception
   *   File could not be saved.
   */
  protected function saveUrls(array $urls, string $destination) {
    $dir = dirname($destination);
    if ($this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {
      $csv_content = implode(PHP_EOL, array_merge(['url'], $urls)) . PHP_EOL;
      if (file_put_contents($this->fileSystem->realpath($destination), $csv_content)) {
        $this->logger->info(sprintf('Files saved at %s', $destination));
        return;
      }
    }

    throw new \Exception('File could not be saved');
  }

}
