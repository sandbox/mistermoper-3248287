<?php

namespace Drupal\wp_migration\Plugin;

use Drupal\Core\Database\Database;

/**
 * Common code to work with translations.
 */
trait TranslationTrait {

  /**
   * Get the id and language code of a specific wp element id.
   */
  protected function getOriginalLanguageInfo($id, $langcode, $prefix) {
    $database = Database::getConnection('default', 'legacy');
    $query = $database->select($prefix . '_icl_translations', 't');
    $query
      ->fields('t', [
        'element_id',
        'source_language_code',
        'language_code',
        'trid',
      ]);
    $query->condition('t.element_id', $id);
    $query->condition('t.language_code', $langcode);
    $result = $query->execute();
    $fields = $result->fetchAllAssoc('element_id', \PDO::FETCH_ASSOC);
    if (empty($fields[$id]['source_language_code'])) {
      return $fields[$id];
    }
    $trid = $fields[$id]['trid'];
    $query2 = $database->select($prefix . '_icl_translations', 't');
    $query2
      ->fields('t', [
        'element_id',
        'language_code',
        'trid',
      ]);
    $query2->condition('t.trid', $trid);
    $query2->isNull('t.source_language_code');
    $result = $query2->execute();
    $fields = $result->fetchAllAssoc('trid', \PDO::FETCH_ASSOC);
    return $fields[$trid];
  }

}
