<?php

namespace Drupal\wp_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\wp_migration\Plugin\TranslationTrait;

/**
 * Provides a wp_translation plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: wp_translation
 *     source: foo
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "wp_translation"
 * )
 *
 * @DCG
 * ContainerFactoryPluginInterface is optional here. If you have no need for
 * external services just remove it and all other stuff except transform()
 * method.
 */
class Translation extends ProcessPluginBase {

  use TranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $id = $value;
    $langcode = $row->get('language_code');
    // @todo better get prefix from source plugin
    $prefix = $this->configuration['table_prefix'];
    $fields = $this->getOriginalLanguageInfo($id, $langcode, $prefix);
    return [$fields['element_id'], $fields['language_code']];
  }

}
