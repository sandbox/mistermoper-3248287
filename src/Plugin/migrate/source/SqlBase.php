<?php

namespace Drupal\wp_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase as DrupalSqlBase;

/**
 * Based table for source wordpress plugin.
 */
abstract class SqlBase extends DrupalSqlBase {

  /**
   * Get database table prefix from the migration template.
   */
  protected function getPrefix() {
    return !empty($this->configuration['table_prefix']) ? $this->configuration['table_prefix'] : 'wp';
  }

}
