<?php

namespace Drupal\wp_migration\Plugin\migrate\source;

/**
 * Extract content from Wordpress site.
 *
 * @MigrateSource(
 *   id = "wordpress_terms"
 * )
 */
class TaxonomyTerms extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $table_prefix = $this->getPrefix();
    $query = $this->select($table_prefix . '_terms', 't');
    $query
      ->fields('t', [
        'term_id',
        'name',
        'slug',
      ])
      ->fields('tt', [
        'taxonomy',
      ])
      ->fields('translations', [
        'source_language_code',
      ]);
    $query->addExpression('IF(language_code != NULL, language_code, :english_language)', 'language_code', [':english_language' => 'en']);

    $query->join($table_prefix . '_term_taxonomy', 'tt', 't.term_id = tt.term_id');
    $query->leftJoin(
      $table_prefix . '_icl_translations',
      'translations',
      'translations.element_type = CONCAT(:tax_prefix, tt.taxonomy) AND t.term_id = translations.element_id',
      [':tax_prefix' => 'tax_']);

    if (isset($this->configuration['taxonomy'])) {
      $query->condition('taxonomy', $this->configuration['taxonomy'], 'IN');
    }

    if (isset($this->configuration['language_code'])) {
      $language_condition = $query->orConditionGroup();
      $language_condition->condition('language_code', $this->configuration['language_code']);
      $language_condition->isNull('language_code');
      $query->condition($language_condition);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'term_id' => $this->t('Term id'),
      'name' => $this->t('Name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'term_id' => [
        'type'  => 'integer',
        'alias' => 't',
      ],
      'language_code' => [
        'type'  => 'language',
        'alias' => 'translations',
      ],
    ];
  }

}
