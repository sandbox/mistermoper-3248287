<?php

namespace Drupal\wp_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\wp_migration\Plugin\TranslationTrait;

/**
 * Extract content from Wordpress site.
 *
 * @MigrateSource(
 *   id = "wordpress_content"
 * )
 */
class Content extends SqlBase {

  use TranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $prefix = $this->getPrefix();
    $query = $this->select($prefix . '_posts', 'p');
    $query
      ->fields('p', [
        'id',
        'post_date',
        'post_title',
        'post_content',
        'post_excerpt',
        'post_modified',
        'post_name',
      ]);
    $query->join($prefix . '_icl_translations', 't', 't.element_id = p.id and t.element_type = :type', [':type' => 'post_post']);
    $query->addField('t', 'language_code');
    $query->condition('p.post_status', 'publish');
    $query->condition('p.post_type', $this->getPostType());
    $query->orderBy('t.language_code', 'ASC');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id'            => $this->t('Post ID'),
      'post_title'    => $this->t('Title'),
    // 'thumbnail'     => $this->t('Post Thumbnail'),
      'post_excerpt'  => $this->t('Excerpt'),
      'post_content'  => $this->t('Content'),
      'post_date'     => $this->t('Created Date'),
      'post_modified' => $this->t('Modified Date'),
      'path_name'     => $this->t('URL Alias'),
      'language_code' => $this->t('Language code'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type'  => 'integer',
        'alias' => 'p',
      ],
      'language_code' => [
        'type'  => 'language',
        'alias' => 't',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $row->setSourceProperty('post_tags', $this->getTaxonomyTags($row, 'post_tag'));
    $row->setSourceProperty('category_tags', $this->getTaxonomyTags($row, 'category'));
  }

  /**
   * Get the related tags with this post.
   *
   * @param \Drupal\migrate\Row $row
   *   Row.
   * @param string $taxonomy
   *   Machine name of the taxonomy.
   *
   * @return array
   *   List of related tags.
   */
  protected function getTaxonomyTags(Row $row, string $taxonomy) {
    $id = $row->get('id');
    $langcode = $row->get('language_code');
    // @todo better get prefix from source plugin
    $table_prefix = $this->getPrefix();

    $element_info = $this->getOriginalLanguageInfo($id, $langcode, $table_prefix);
    $trid = $element_info['trid'];

    $query = $this->select($table_prefix . '_term_relationships', 'tr')
      ->fields('tr', ['term_taxonomy_id']);

    $query->join($table_prefix . '_term_taxonomy', 'tt', 'tr.term_taxonomy_id = tt.term_id');

    $query->join($table_prefix . '_icl_translations', 'it', 'it.element_id = tr.object_id');

    $group_or_id_filter = $query->orConditionGroup();
    $group_or_id_filter->condition('it.trid', $trid);
    $group_or_id_filter->condition('it.element_id', $trid);
    $query->condition($group_or_id_filter);

    $query->condition('tt.taxonomy', $taxonomy);

    $tags = array_values($query->execute()->fetchAllKeyed(0, 0));
    return $tags;
  }

  /**
   * Get post thumbnail.
   */
  protected function getPostThumbnail(Row $row) {
    $prefix = $this->getPrefix();
    $query = $this->select($prefix . '_postmeta', 'pm');
    $query->innerJoin($prefix . '_postmeta', 'pm2', 'pm2.post_id = pm.meta_value');
    $query->fields('pm', ['post_id'])
      ->condition('pm.post_id', $row->getSourceProperty('id'))
      ->condition('pm.meta_key', '_thumbnail_id')
      ->condition('pm2.meta_key', '_wp_attached_file');
    return $query->execute()->fetchField();
  }

  /**
   * Get Wordpress post type from the migration template.
   */
  protected function getPostType() {
    return !empty($this->configuration['post_type']) ? $this->configuration['post_type'] : 'post';
  }

}
